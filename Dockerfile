FROM golang:latest
ADD . /go/
WORKDIR /go
RUN go get -d ./...
RUN go build main.go
EXPOSE 8080
CMD go run main.go